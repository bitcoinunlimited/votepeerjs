const fs = require('fs')

const build_number = process.env.CI_PIPELINE_IID
if (!build_number) {
    console.error("Please set environment variable CI_PIPELINE_IID");
    process.exit(1)
}

const rawData = fs.readFileSync('package.json');
const json = JSON.parse(rawData);
version = json["version"].split(".")
json["version"] = version[0] + "." + version[1] + "." + build_number
const data = JSON.stringify(json, null, 2);
fs.writeFileSync('package.json', data);

console.log("Build number set to", json["version"])
