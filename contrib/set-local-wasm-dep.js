const fs = require('fs')

const rawData = fs.readFileSync('package.json');
const json = JSON.parse(rawData);
json["dependencies"]["fujisaki-ringsig-wasm"] = "file:wasm/fujisaki-ringsig-wasm/pkg/"
const data = JSON.stringify(json, null, 2);
fs.writeFileSync('package.json', data);

console.log(
    "fujisaki-ringsig-wasm dependency set to",
    json["dependencies"]["fujisaki-ringsig-wasm"])
