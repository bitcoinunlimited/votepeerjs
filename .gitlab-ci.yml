image: node:latest

#
# Dependencies
#
build-wasm:
    image: rust:buster
    needs: []
    variables:
        CARGO_HOME: $CI_PROJECT_DIR/.cargo
        APT_CACHE: $CI_PROJECT_DIR/.apt-cache
    cache:
        paths:
            - .cargo
            - .apt-cache
    before_script:
        - mkdir -p $APT_CACHE
    script:
        # Install wasm-pack
        - curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh

        # Run build from npm
        - apt-get update -q > /dev/null
        - apt-get -q
            -o dir::cache::archives="$APT_CACHE"
            -y install nodejs npm > /dev/null
        - npm run build:ringsig

    after_script:
        # Supposedly it's cheaper to re-download than cache the registry index.
        - rm -rf .cargo/registry/index

    artifacts:
        paths:
            - wasm/fujisaki-ringsig-wasm/pkg

install-dependencies:
    needs: []
    cache:
        paths:
            - node_modules/
    script:
        - npm install
    artifacts:
        paths:
            - node_modules/

install-dependencies-with-wasm:
    needs: [build-wasm]
    dependencies:
        - build-wasm
    cache:
        paths:
            - node_modules/
    script:
        # Install fujisaki-ringsig-wasm built in step 'build-wasm'
        - node contrib/set-local-wasm-dep.js
        - rm -rf node_modules/fujisaki-ringsig-wasm
        - npm install
    artifacts:
        paths:
            - package.json
            - node_modules/

examples-install-dependencies:
    needs: [compile-main, generate-pack]
    dependencies:
        - compile-main
        - generate-pack
    cache:
        paths:
            - examples/node_modules/
    script:
        - (cd examples; npm install)
        # npm install just does a symlink, which won't work as artifact,
        # so we need to install the package.
        - (cd examples; rm -rf node_modules/@bitcoinunlimited/votepeerjs)
        - (cd examples; npm install ../packs/`ls ../packs | head -n1`)
    artifacts:
        paths:
            - examples/node_modules/
#
# Static checks
#

lint-code:
    needs: [install-dependencies]
    dependencies:
        - install-dependencies
    script:
        - npm run lint

spellcheck:
    needs: [install-dependencies]
    dependencies:
        - install-dependencies
    script:
        - npm run spellcheck

#
# Build tasks
#
compile-main:
    needs: [install-dependencies]
    script:
        - npm run compile:main
    artifacts:
        paths:
            - dist/main
            - node_modules
    dependencies:
        - install-dependencies

compile-module:
    needs: [install-dependencies]
    script:
        - npm run compile:module
    artifacts:
        paths:
            - dist/module
            - node_modules
    dependencies:
        - install-dependencies

compile-with-wasm:
    needs: [install-dependencies-with-wasm]
    script:
        - npm run build
    artifacts:
        paths:
            - dist
            - node_modules
            - package.json
    dependencies:
        - install-dependencies-with-wasm

examples-build:
    needs: [examples-install-dependencies]
    dependencies:
        - examples-install-dependencies
    script:
        - (cd examples; npm run build)
    artifacts:
        paths:
            - examples/build
#
# Test tasks
#
test:
    needs: [compile-main]
    script:
        - npm run test:jest-report
    dependencies:
        - compile-main
    artifacts:
        when: always
        reports:
            junit:
                - junit.xml

test-with-wasm:
    needs: [compile-with-wasm]
    script:
        - npm run test:jest-report
    dependencies:
        - compile-with-wasm
    artifacts:
        when: always
        reports:
            junit:
                - junit.xml

test-importing-module:
    needs: [generate-pack]
    dependencies: [generate-pack]
    script:
        - node contrib/test-installation-pack.js

#
# Generate artifacts
#
generate-typedoc:
    needs: [install-dependencies]
    dependencies:
        - install-dependencies
    script:
        - npm install -g typedoc
        - npm run doc
    artifacts:
        paths:
            - reference

generate-mkdocs:
    image: python:buster
    needs: []
    variables:
        PIP_CACHE: $CI_PROJECT_DIR/.pipcache
    cache:
        key: mkdocs
        paths:
            - .pipcache
    before_script:
        - pip install mkdocs
        - pip install mkdocs-material
    script:
        - cp README.md doc/
        - mkdocs build
    artifacts:
        paths:
            - site

generate-pack:
    dependencies: [compile-main, compile-module]
    needs: [compile-main, compile-module]
    script:
        - npm run pack
    artifacts:
        paths:
            - packs

#
# Publish
#
pages:
    image: alpine:latest
    dependencies:
        - generate-typedoc
        - generate-mkdocs
    needs: [generate-typedoc, generate-mkdocs, spellcheck]
    script:
        - mv site public
        - mv reference public/reference
    artifacts:
        paths:
            - public
    rules:
        - if: $CI_COMMIT_BRANCH == "master"

publish-npmjs:
    dependencies: [compile-main, compile-module]
    # Don't run unless all test & linter succeed.
    needs: [
        compile-main, compile-module,
        lint-code, spellcheck,
        test, test-with-wasm, test-importing-module,
        examples-build
    ]
    script:
        - node contrib/set-version-build.js
        - echo "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" > .npmrc
        - npm publish --access public
    rules:
        - if: $CI_COMMIT_BRANCH == "master"
