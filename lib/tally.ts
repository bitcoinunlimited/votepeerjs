import NetworkTransaction from './networktransaction';

/**
 * Transaction with a valid vote.
 */
export type AcceptedBallotTransaction = {
    transaction: NetworkTransaction,
    vote: Uint8Array,
    signature: Uint8Array,
    participant: string | null,
};
/**
 * Transaction with an invalid vote.
 */
export type RejectedBallotTransaction = {
    transaction: NetworkTransaction,
    reason: string
    participant: string | null,
};

/**
 * All vote casting transactions for an election.
 */
export type BallotResult = {
    accepted: AcceptedBallotTransaction[],
    rejected: RejectedBallotTransaction[],
};
