import InvalidVoteError from './invalidvoteerror';

export default class NoVoteError extends InvalidVoteError {
  constructor() {
    super('No vote cast');
  }
}
