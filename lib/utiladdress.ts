import {
  bigIntToBinUint64LE, cashAddressToLockingBytecode, encodeDataPush,
  encodeTransaction, generateTransaction, getTransactionHashBE,
} from '@bitauth/libauth';
import Blockchain from './blockchain';
import { derivePublicKey, getPublicKeyHash, sha256Hasher } from './crypto';
import { DEFAULT_DUST_THRESHOLD } from './networkconstants';
import { getTxID, hashToAddress, signTransactionInput } from './transaction';

/**
 * Get a UTXO that spends from address with at least `minimumSatoshis`. If one
 * exists it is returned, otherwise it is created.
 *
 * @param electrum - Electrum connection
 * @param address - Address to send funds to
 * @param minimumSatoshis - Minimum amount of satoshis to send
 * @param p2pkhPrivateKey - Private key to a P2PKH address to fund from.
 * @returns utxo
 */
export async function getOrFundUtxo(
  electrum: Blockchain,
  address: string,
  minimumSatoshis: number,
  p2pkhPrivateKey: Uint8Array,
): Promise<{
      outpointIndex: number,
      outpointTransactionHash: Uint8Array,
      satoshis: number,
    }> {
  for (const utxo of await electrum.listUnspent(address)) {
    if (utxo.value < minimumSatoshis) {
      continue;
    }
    const outpointTransactionHash = Uint8Array.from(Buffer.from(utxo.tx_hash, 'hex'));
    return {
      outpointIndex: utxo.tx_pos,
      outpointTransactionHash,
      satoshis: utxo.value,
    };
  }

  // Fund vote
  const fundingPublicKey = await derivePublicKey(p2pkhPrivateKey);
  const fundingPKH = await getPublicKeyHash(fundingPublicKey);
  const fundingAddress = hashToAddress(fundingPKH, 'p2pkh');
  const feeEstimate = 350;
  const utxo = await electrum.getUtxo(fundingAddress, minimumSatoshis + feeEstimate);

  const lockingScript: Uint8Array = (cashAddressToLockingBytecode(address) as any).bytecode;
  const outputs = [{
    lockingBytecode: lockingScript,
    satoshis: bigIntToBinUint64LE(BigInt(minimumSatoshis)),
  }];

  const change = utxo.satoshis - (minimumSatoshis + feeEstimate);
  const funderLockingScript: Uint8Array = (cashAddressToLockingBytecode(fundingAddress) as any)
    .bytecode;

  if (change >= DEFAULT_DUST_THRESHOLD) {
    outputs.push({
      lockingBytecode: funderLockingScript,
      satoshis: bigIntToBinUint64LE(BigInt(change)),
    });
  }

  const fundingTx = generateTransaction({
    inputs: [{
      outpointIndex: utxo.outpointIndex,
      outpointTransactionHash: utxo.outpointTransactionHash,
      unlockingBytecode: Buffer.alloc(0),
      sequenceNumber: 0,
    }],
    locktime: 0,
    version: 2,
    outputs,
  });

  if (!fundingTx.success) {
    throw fundingTx.errors;
  }

  const txSig = await signTransactionInput(
    fundingTx.transaction,
    utxo.satoshis,
    0,
    funderLockingScript,
    p2pkhPrivateKey,
  );

  // p2pkh unlocking bytecode
  fundingTx.transaction.inputs[0].unlockingBytecode = Buffer.concat([
    encodeDataPush(txSig),
    encodeDataPush(fundingPublicKey),
  ]);
  const fundingTxID = await getTxID(fundingTx.transaction);
  const serializedTx = encodeTransaction(fundingTx.transaction);
  const broadcastTxID = await electrum.broadcast(serializedTx);
  if (broadcastTxID !== fundingTxID) {
    console.warn(`Electrum returned different txid, expected ${fundingTxID}, got ${broadcastTxID}`);
  }
  return {
    outpointIndex: 0,
    outpointTransactionHash: getTransactionHashBE(await sha256Hasher(), serializedTx),
    satoshis: minimumSatoshis,
  };
}

/**
  * Wait for contract to contain at minimum this balance.
  *
  * @param address - Address to query balance from.
  * @param minimumBalance - Minimum balance in satoshis before returning.
  * @param waitForBalance - Called at intervals while waiting on balance.
  */
export async function waitForBalance(
  blockchain: Blockchain,
  address: string,
  minimumBalance: number,
  waitCallback: (balance: number) => void,
): Promise<number> {
  let balance = 0;
  /* eslint no-constant-condition: ["error", { "checkLoops": false }] */
  while (true) {
    balance = await blockchain.getAddressBalance(address);
    if (balance >= minimumBalance) {
      break;
    }
    waitCallback(balance);
  }
  return balance;
}
