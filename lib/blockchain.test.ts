import Blockchain from './blockchain';
import NetworkTransaction from './networktransaction';

const BU_DONATION_ADDRESS = 'bitcoincash:pq6snv5fcx2fp6dlzg7s0m9zs8yqh74335tzvvfcmq';

test('getSpendingTxs', async () => {
  jest.setTimeout(30000);
  expect.assertions(4);
  const chain = new Blockchain();
  await chain.connect();

  const res = await chain.getSpendingTxs(
    'bitcoincash:qrfat0epsxsvq0tgdve74r9tuu5cqhx0hq9y9vzsva', 0, 623176, false,
  );

  expect(res.length).toEqual(3);

  const ids: string[] = [];
  for (const tx of res) {
    ids.push(tx.getTxID());
  }
  ids.sort();

  expect(ids[0]).toEqual('12c99127e82856c9c3e8532857e49e38e0e1a0d1f1a7bd988dc1e8749d3094d4');
  expect(ids[1]).toEqual('447ff6da6ced10ba036396779550b2a7843aabee6c4a2a562090070da79d7230');
  expect(ids[2]).toEqual('60caadfa183c63192dd8806c7cfc3918bce13c58cfb104e010c6623b36b26a17');

  chain.disconnect();
});

test('getSpendingTxs_heightFilter', async () => {
  jest.setTimeout(30000);
  expect.assertions(3);
  const chain = new Blockchain();
  await chain.connect();

  const addr = 'bitcoincash:qrskglyrvfk9jz6hcnaxcm8852eskzazhgq4r0mf8g';
  const confirmHeight = 624858;
  let res = await chain.getSpendingTxs(addr, 0, confirmHeight - 1, true);
  expect(0).toEqual(res.length);
  res = await chain.getSpendingTxs(addr, 0, confirmHeight, true);
  expect(2).toEqual(res.length);
  res = await chain.getSpendingTxs(addr, 0, confirmHeight + 1, true);
  expect(2).toEqual(res.length);
  chain.disconnect();
});

test('getFundingTransactions', async () => {
  jest.setTimeout(30000);
  const chain = new Blockchain();
  await chain.connect();

  const minHeight = 696925;
  const maxHeight = 712598;
  const txs = await chain.getFundingTransactions(
    BU_DONATION_ADDRESS, minHeight, maxHeight,
  );
  expect((txs.at(0) as NetworkTransaction).height).toBe(minHeight);
  expect((txs.at(-1) as NetworkTransaction).height).toBe(maxHeight);
  expect((txs.at(0) as NetworkTransaction).getTxID())
    .toBe('173c6bb1fb8489e665fa74468290cead23a7e2338b4b7df700323d57867319dd');
  expect((txs.at(-1) as NetworkTransaction).getTxID())
    .toBe('2c75bd1936b5b0690a4f5ce8bc0927712b193b46c20049c6edac306273e58f00');
  expect(txs.length).toBe(9);
  chain.disconnect();
});
