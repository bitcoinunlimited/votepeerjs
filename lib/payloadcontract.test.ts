import { binToHex, generateTransaction, encodeDataPush } from '@bitauth/libauth';
import assert from 'assert';
import { PayloadContract } from './payloadcontract';
import {
  SCHNORR_SIGNATURE_SIZE,
  PUBLIC_KEY_COMPACT_SIZE,
  MAX_TX_IN_SCRIPT_SIG_SIZE,
} from './networkconstants';
import { generatePrivateKey, derivePublicKey } from './crypto';
import InvalidPayloadError from './error/invalidpayloaderror';

test('testPayload', async () => {
  /**
     * Test that when input is enough, but low, that all coins are sent to the
     * contract.
     */
  const signature = new Uint8Array(SCHNORR_SIGNATURE_SIZE);
  const pubKey = new Uint8Array(PUBLIC_KEY_COMPACT_SIZE);
  const payload = new Uint8Array(PayloadContract.MAX_PAYLOAD_PER_INPUT_SIZE);
  signature.fill(0xaa);
  pubKey.fill(0xbb);
  payload.fill(0xcc);

  const payloadHash = await PayloadContract.calculatePayloadHash(pubKey, payload);
  const redeemScript = PayloadContract.redeemScript(pubKey, payloadHash);

  expect(redeemScript.length).toEqual(71);

  const unlockingScript = await PayloadContract.unlockingScript(
    signature, pubKey,
    payload,
  );

  expect(unlockingScript.length).toBe(MAX_TX_IN_SCRIPT_SIG_SIZE);
});

test('testPayloadToStackItems', async () => {
  const push1 = new Uint8Array(PayloadContract.MAX_PUSH1_SIZE);
  const push2 = new Uint8Array(PayloadContract.MAX_PUSH2_SIZE);
  const push3 = new Uint8Array(PayloadContract.MAX_PUSH3_SIZE);
  push1.fill(0xaa);
  push2.fill(0xbb);
  push3.fill(0xcc);
  const payload = Buffer.concat([push1, push2, push3]);

  const [push1A, push2A, push3A] = PayloadContract.payloadToStackItems(payload);

  expect(Buffer.from(push1).equals(push1A)).toBe(true);
  expect(Buffer.from(push2).equals(push2A)).toBe(true);
  expect(Buffer.from(push3).equals(push3A)).toBe(true);

  const p2 = new Uint8Array(42);
  p2.fill(0xff);

  const [push1B, push2B, push3B] = PayloadContract.payloadToStackItems(
    Buffer.concat([push1, p2]),
  );

  expect(Buffer.from(push1).equals(push1B)).toBe(true);
  expect(Buffer.from(p2).equals(push2B)).toBe(true);
  expect(push3B.length).toBe(0);
});

test('testCalcPayloadHash', async () => {
  const payload = new Uint8Array(PayloadContract.MAX_PAYLOAD_PER_INPUT_SIZE);
  const pubKey = new Uint8Array(33);
  payload.fill(0xaa);
  pubKey.fill(0xbb);
  const hash = await PayloadContract.calculatePayloadHash(pubKey, payload);

  expect(binToHex(hash)).toBe('3751c49910a95c3a221e9374c208e6ea507a3de9');
});

function dummyTxWithInput(unlockingScript: Uint8Array) {
  const result: any = generateTransaction({
    inputs: [{
      outpointIndex: 0,
      outpointTransactionHash: new Uint8Array(32),
      unlockingBytecode: unlockingScript,
      sequenceNumber: 0,
    }],
    locktime: 0,
    outputs: [],
    version: 2,
  });
  assert(result.success);
  return result.transaction;
}

test('testGetPayloadFromTx', async () => {
  const publicKey = await derivePublicKey(generatePrivateKey());
  const signature = new Uint8Array(SCHNORR_SIGNATURE_SIZE);
  signature.fill(0xaa);
  const payload = Buffer.from('a tiny payload');

  const unlockingScript = await PayloadContract.unlockingScript(
    signature, publicKey, payload,
  );

  const tx = dummyTxWithInput(unlockingScript);
  const payloadFromTx = await PayloadContract.getPayloadFromTx(tx);
  expect(Buffer.from(payload).equals(payloadFromTx)).toBe(true);
});

test('testGetPayloadFromTxInvalid', async () => {
  const publicKey = await derivePublicKey(generatePrivateKey());
  const signature = new Uint8Array(SCHNORR_SIGNATURE_SIZE);
  signature.fill(0xaa);
  const payload = Buffer.from('a tiny payload');

  const [push1, push2, push3] = PayloadContract.payloadToStackItems(payload);

  const invalidPayloadHash = new Uint8Array(PayloadContract.PAYLOAD_HASH_SIZE);
  const redeemScript = await PayloadContract.redeemScript(publicKey, invalidPayloadHash);

  const unlockingScript = Buffer.concat([
    encodeDataPush(signature),
    encodeDataPush(push1),
    encodeDataPush(push2),
    encodeDataPush(push3),
    encodeDataPush(redeemScript),
  ]);
  const tx = dummyTxWithInput(unlockingScript);
  const t = async () => {
    await PayloadContract.getPayloadFromTx(tx);
  };
  await expect(t()).rejects.toThrow(InvalidPayloadError);
  await expect(t()).rejects.toThrow(/Payload Hash mismatch/);

  // Test that we detect modified redeemscript

  const payloadHash = await PayloadContract.calculatePayloadHash(publicKey, payload);
  const validRedeemScript = await PayloadContract.redeemScript(publicKey, payloadHash);
  const invalidRedeemScript = Buffer.concat([validRedeemScript, Buffer.from('garbage')]);

  const unlockingScript2 = Buffer.concat([
    encodeDataPush(signature),
    encodeDataPush(push1),
    encodeDataPush(push2),
    encodeDataPush(push3),
    encodeDataPush(invalidRedeemScript),
  ]);

  const tx2 = dummyTxWithInput(unlockingScript2);
  const t2 = async () => {
    await PayloadContract.getPayloadFromTx(tx2);
  };
  await expect(t2()).rejects.toThrow(InvalidPayloadError);
  await expect(t2()).rejects.toThrow(/Incorrect redeem/);
});
