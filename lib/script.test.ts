import { OpcodesCommon, encodeDataPush } from '@bitauth/libauth';
import { getDirectPushOP, copyStackElementAt, LAST_DATA_PUSH } from './script';

describe('script', () => {
  test('testGetDirectPushOP', () => {
    expect(getDirectPushOP(OpcodesCommon.OP_0)).toStrictEqual(Uint8Array.from([]));
    expect(getDirectPushOP(OpcodesCommon.OP_16)).toStrictEqual(Uint8Array.from([16]));
    expect(getDirectPushOP(OpcodesCommon.OP_1NEGATE)).toStrictEqual(Uint8Array.from([-1]));

    // outside direct push range
    expect(getDirectPushOP(OpcodesCommon.OP_1NEGATE - 1)).toBe(null);
    expect(getDirectPushOP(OpcodesCommon.OP_16 + 1)).toBe(null);
  });

  test('testCopyStackElementAtZero', () => {
    // Test cases

    // special case with empty result
    const directPushData0 = Buffer.alloc(1, OpcodesCommon.OP_0);
    const directPushData16 = Buffer.alloc(1, OpcodesCommon.OP_16);

    const push0Data = Uint8Array.from(Buffer.alloc(42, 0xaa));

    const push1Data = Uint8Array.from(Buffer.alloc(LAST_DATA_PUSH + 1, 0xaa));
    const push2Data = Uint8Array.from(Buffer.alloc(0xff + 1, 0xaa));
    // check that byte 2 is decoded properly (LE vs BE)
    const push2Data2 = Uint8Array.from(Buffer.alloc(0xffee, 0xaa));

    // tests
    const [directResult0, directStart0, directEnd0] = copyStackElementAt(directPushData0, 0);
    expect(directResult0.length).toBe(0);
    expect(directStart0).toBe(0);
    expect(directEnd0).toBe(0);

    const [directResult16, directStart16, directEnd16] = copyStackElementAt(directPushData16, 0);
    expect(directResult16).toStrictEqual(Uint8Array.from([16]));
    expect(directStart16).toBe(0);
    expect(directEnd16).toBe(1);

    const [push0Result, push0Start, push0End] = copyStackElementAt(encodeDataPush(push0Data), 0);
    expect(push0Result).toStrictEqual(push0Data);
    expect(push0Start).toBe(1);
    expect(push0End).toBe(1 + push0Data.length - 1);

    const [push1Result, push1Start, push1End] = copyStackElementAt(encodeDataPush(push1Data), 0);
    expect(push1Result).toStrictEqual(push1Data);
    expect(push1Start).toBe(2);
    expect(push1End).toBe(2 + push1Data.length - 1);

    const [push2Result, push2Start, push2End] = copyStackElementAt(encodeDataPush(push2Data), 0);
    expect(push2Result).toStrictEqual(push2Data);
    expect(push2Start).toBe(3);
    expect(push2End).toBe(3 + push2Data.length - 1);

    expect(copyStackElementAt(encodeDataPush(push2Data2), 0)[0]).toStrictEqual(push2Data2);

    // should ignore additional pushes
    const multiPush = Uint8Array.from(Buffer.concat([
      encodeDataPush(push2Data),
      encodeDataPush(push0Data),
    ]));
    expect(copyStackElementAt(multiPush, 0)[0]).toStrictEqual(push2Data);

    // special case: empty element
    const [emptyResult, emptyStart, emptyEnd] = copyStackElementAt(
      encodeDataPush(Uint8Array.from([])), 0,
    );
    expect(emptyResult.length).toBe(0);
    expect(emptyStart).toBe(0);
    expect(emptyEnd).toBe(0);
  });

  test('testCopyStackElementAtOffset', () => {
    const push1Data = Buffer.alloc(42, 0xaa);
    const push2Data = Buffer.alloc(0xff + 1, 0xab);

    const multiPush = Buffer.concat([
      encodeDataPush(push1Data),
      encodeDataPush(push2Data),
    ]);
    const push1End = copyStackElementAt(multiPush, 0)[2];
    expect(push1End).toBe(42);
    const [push2Result, push2Start, push2End] = copyStackElementAt(multiPush, push1End + 1);
    expect(push2Result).toStrictEqual(push2Data);
    expect(push2Start).toBe(46);
    expect(push2End).toBe(multiPush.length - 1);
  });
});
