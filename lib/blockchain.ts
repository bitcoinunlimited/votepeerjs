import assert from 'assert';
import { ElectrumClient } from 'electrum-cash';
import { decodeTransactionUnsafe, Transaction } from '@bitauth/libauth';
import { toOutputScript } from './transaction';
import NetworkTransaction from './networktransaction';

/**
 * We don't do SPV verification (yet?), so this needs to be a server that
 * does not lie to us.
 */
const SERVERS_WE_TRUST = ['electrs.bitcoinunlimited.info', 'bitcoincash.network'];

declare type RPCParameter = string | number | boolean | null;

function isOutputTo(output: any, addr: string): boolean {
  const script = toOutputScript(addr);
  return output.lockingBytecode.compare(script) === 0;
}

export default class Blockchain {
    electrum: ElectrumClient | null;

    servers: string[] = SERVERS_WE_TRUST;

    constructor(servers?: string[]) {
      if (servers !== undefined) {
        this.servers = servers;
      }
      this.electrum = null;
    }

    public async connect(): Promise<void> {
      if (this.electrum !== null) {
        return;
      }
      for (const server of this.servers) {
        try {
          this.electrum = new ElectrumClient('The best client', '1.4', server);
          await this.electrum.connect();
          break;
        } catch (error) {
          console.error(`Failed to connect to server: ${server}`, error);
          this.electrum = null;
          continue;
        }
      }
      if (this.electrum === null) {
        throw new Error('Failed to connect to all servers');
      }
    }

    public disconnect() {
      assert(this.electrum !== null);
      this.electrum.disconnect();
    }

    public async hasInputFrom(tx: any, addr: string): Promise<boolean> {
      for (const input of tx.inputs) {
        const txhash = input.outpointTransactionHash.toString('hex');

        const inputTx: any = await this.getTx(txhash);
        if (isOutputTo(inputTx.outputs[input.outpointIndex], addr)) {
          return true;
        }
      }
      return false;
    }

    public async getTx(txid: string): Promise<Transaction> {
      return decodeTransactionUnsafe(await this.getTransactionSerialized(txid));
    }

    public async getTransactionSerialized(txid: string): Promise<Uint8Array> {
      assert(this.electrum !== null);
      const txHex = await this.electrum.request(
        'blockchain.transaction.get', txid,
      );
      if (typeof (txHex) !== 'string') {
        throw txHex;
      }
      return Buffer.from(txHex, 'hex');
    }

    public async getFundingTransactions(
      address: string,
      minBlockHeight: number | null = null,
      maxBlockHeight: number | null = null,
      includeUnconfirmed: boolean = true,
    ): Promise<NetworkTransaction[]> {
      const [history, tipHeight] = await Promise.all([
        this.request('blockchain.address.get_history', address),
        this.getBlockchainTipHeight(),
      ]);
      assert(tipHeight !== undefined);

      const fetchTxQueue = [];

      for (const entry of history) {
        if (maxBlockHeight !== null
            && entry.height > maxBlockHeight) {
          continue;
        }

        const isConfirmed = entry.height > 0;
        if (!includeUnconfirmed && !isConfirmed) {
          continue;
        }

        if (minBlockHeight !== null
            && isConfirmed && entry.height < minBlockHeight) {
          continue;
        }

        if (maxBlockHeight !== null
            && !isConfirmed && tipHeight >= maxBlockHeight) {
          // Above max height. Unconfirmed txs will not confirm below maxHeight.
          continue;
        }

        fetchTxQueue.push([entry, this.getTransactionSerialized(entry.tx_hash)]);
      }

      const txs: Promise<NetworkTransaction>[] = [];
      while (fetchTxQueue.length !== 0) {
        const [entry, txPromise] = fetchTxQueue.shift() as any[];
        const txSerialized = await txPromise;
        const tx = decodeTransactionUnsafe(txSerialized);
        for (const o of tx.outputs) {
          if (!isOutputTo(o, address)) {
            continue;
          }
          txs.push(NetworkTransaction.make(txSerialized, entry.height, entry.tx_hash));
          break;
        }
      }
      return Promise.all(txs);
    }

    /**
     * Get transactions that _spend_ from address.
     *
     * @param addr - Bitcoin Cash address
     * @param minHeight - Filter out transactions confirmed before this height.
     * @param maxheight - Filter out transactions confirmed after this height.
     * @param includeUnconfirmed - Include unconfirmed (when maxheight \> tip)
     */
    public async getSpendingTxs(addr: string, minHeight: number, maxHeight: number,
      includeUnconfirmed: boolean): Promise<NetworkTransaction[]> {
      const tipHeight = await this.getBlockchainTipHeight();
      if (tipHeight < (minHeight - 1)) {
        // Have not reached min height yet, and min height is not next block, so
        // we assume any unconfirmed votes will confirm before min height.
        // No need to request tx history.
        return [];
      }
      const history = await this.request('blockchain.address.get_history', addr);
      const fetchTxQueue: any[] = [];
      for (const entry of history) {
        // Check if tx is unconfirmed.
        // * 0 means unconfirmed.

        // * -1 means unconfirmed with unconfirmed parent.
        if (entry.height <= 0) {
          if (!includeUnconfirmed) {
            continue;
          }
          if (tipHeight >= maxHeight) {
            // Above max height. Cannot include unconfirmed txs.
            continue;
          }
        }
        if (entry.height > maxHeight) {
          continue;
        }
        if (entry.height > 0 && entry.height < minHeight) {
          continue;
        }
        fetchTxQueue.push([
          entry,
          this.getTransactionSerialized(entry.tx_hash),
        ]);
      }

      const checkInputQueue = [];
      while (fetchTxQueue.length !== 0) {
        const [entry, txPromise] = fetchTxQueue.shift() as any[];
        const txSerialized = await txPromise;
        const tx = decodeTransactionUnsafe(txSerialized);
        checkInputQueue.push([
          entry,
          txSerialized,
          this.hasInputFrom(tx, addr),
        ]);
      }

      const txs: NetworkTransaction[] = [];
      while (checkInputQueue.length !== 0) {
        const [entry, txSerialized, hasInputFrom] = checkInputQueue.shift() as any[];
        if (!await hasInputFrom) {
          continue;
        }
        txs.push(await NetworkTransaction.make(
          txSerialized,
          entry.height,
          entry.tx_hash,
        ));
      }
      return txs;
    }

    public async getBlockchainTipHeight(): Promise<number> {
      const header = await this.request('blockchain.headers.subscribe');

      // We don't actually want events on new blocks. We just wanted the tip.
      // So unsubscribe immediately.
      try {
        await this.request('blockchain.headers.unsubscribe');
      } catch {
        // ignore error
      }

      return header.height;
    }

    public async ping(): Promise<void> {
      return this.request('server.ping');
    }

    public async getAddressBalance(address: string): Promise<number> {
      const balance = await this.request(
        'blockchain.address.get_balance',
        address,
      );

      return balance.confirmed + balance.unconfirmed;
    }

    /**
     * Find a utxo with minimum this amount.
     *
     * @param minimumAmount - Minimum amount in UTXO
     */
    async getUtxo(address: string, minimumAmount: number): Promise<{
      outpointIndex: number,
      outpointTransactionHash: Uint8Array,
      satoshis: number,
    }> {
      if (this.electrum === null) {
        throw Error('Not connected;');
      }
      const utxos = await this.request(
        'blockchain.address.listunspent', address,
      );
      if (utxos.length === 0) {
        throw Error('Address has no funds');
      }
      for (const u of utxos) {
        if (u.value < minimumAmount) {
          continue;
        }

        return {
          outpointIndex: u.tx_pos,
          outpointTransactionHash: Uint8Array.from(Buffer.from(u.tx_hash, 'hex')),
          satoshis: u.value,
        };
      }
      throw new Error(`Address has no utxos with minimum ${minimumAmount} satoshis`);
    }

    async request(method: string, ...parameters: RPCParameter[]): Promise<any> {
      if (this.electrum === null) {
        throw Error('Not connected');
      }
      const response = await this.electrum.request(method, ...parameters);
      if (response instanceof Error) {
        throw response;
      }
      return response;
    }

    /**
     * Broadcast a transaction to the network
     *
     * @param serializedTransaction - Serialized transaction
     * @returns txid
     */
    async broadcast(serializedTransaction: Uint8Array): Promise<string> {
      return this.request(
        'blockchain.transaction.broadcast',
        Buffer.from(serializedTransaction).toString('hex'),
      );
    }

    async listUnspent(address: string): Promise<any> {
      return this.request(
        'blockchain.address.listunspent', address,
      );
    }
}
