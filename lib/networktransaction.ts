import {
  hexToBin, binToHex, decodeTransactionUnsafe, Transaction, getTransactionHashBE,
} from '@bitauth/libauth';
import { sha256Hasher } from './crypto';

/**
 * Transaction that has been broadcasted to the network.
 */
export default class NetworkTransaction {
    static HEIGHT_UNCONFIRMED = 0;

    static HEIGHT_UNCONFIRMED_WITH_PARENT = -1;

    public readonly txSerialized: Uint8Array;

    // Transaction ID as big-endian
    public readonly txIdBE: Uint8Array;

    public readonly height: number;

    private constructor(
      txSerialized: Uint8Array,
      height: number,
      txIdBE: Uint8Array,
    ) {
      this.txSerialized = txSerialized;
      this.height = height;
      this.txIdBE = txIdBE;
    }

    static async make(
      txSerialized: Uint8Array,
      height: number,
      txID: string | null = null,
    ) {
      if (txID === null) {
        const txIdBE = getTransactionHashBE(await sha256Hasher(), txSerialized);
        return new NetworkTransaction(txSerialized, height, txIdBE);
      }
      const txIdBE = hexToBin(txID);
      return new NetworkTransaction(txSerialized, height, txIdBE);
    }

    getTxID(): string {
      return binToHex(this.txIdBE);
    }

    getTransaction(): Transaction {
      return decodeTransactionUnsafe(this.txSerialized);
    }
}
