import { hashToAddress } from './transaction';

test('hashToAddress', () => {
  expect(hashToAddress(Buffer.from('ece079fd8364c0db3c202560110d06063a6152c1', 'hex'), 'p2sh'))
    .toEqual('bitcoincash:prkwq70asdjvpkeuyqjkqygdqcrr5c2jcy9aav5fpz');
  expect(hashToAddress(Buffer.from('09cbf1886934f0f25e3133fb7d4c34848fb2d41a', 'hex'), 'p2pkh'))
    .toEqual('bitcoincash:qqyuhuvgdy60puj7xyelkl2vxjzglvk5rg9elmke8u');
});
