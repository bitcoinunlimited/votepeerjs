import Blockchain from '../blockchain';
import { MultiOptionVote } from './multioptionvote';
import { hashFromAddress } from '../transaction';
import { fetchMultiOptionVotes } from './multioptionvotetally';

describe('multiOptionVoteTallyTests', () => {
  test('tally', async () => {
    const election = new MultiOptionVote(
    // Salt is *not* hex encoded.
      Buffer.from('4f06221a3d48da3a2965d665ee5d8b0441cfb57b65f6b7819620e391b49fc66c'),
      'Test MOV #2',
      732337,
      736657,
      ['Accept', 'Reject'],
      [
        hashFromAddress('bitcoincash:qqth3xv8z6aysxedsmwcspgkrwt7lm9whyr5fmawvc'),
        hashFromAddress('bitcoincash:qpxt75avxax8uakv47wg46rkp6ay72pnkqq5z2t325'),
        hashFromAddress('bitcoincash:qqs37r80l0enh2lykdq4cxdtz57y0amh9ss68thdye'),
      ],
    );
    const electrum = new Blockchain();
    await electrum.connect();

    const ballots = await fetchMultiOptionVotes(electrum, election);
    expect(ballots.accepted.length).toBe(1);
    expect(ballots.accepted[0].transaction.getTxID()).toBe('287d63da57800f26b2ef824aab7422c586e5eeebbbcd59321c5cf7ead0c7d03e');
    expect(ballots.accepted[0].vote)
      .toStrictEqual(Buffer.from((await election.voteOptionsHashed)[1]));
    expect(ballots.accepted[0].participant).toBe('bitcoincash:qqs37r80l0enh2lykdq4cxdtz57y0amh9ss68thdye');
    electrum.disconnect();
  });
});
