/**
 * The magical 20 bytes representing a blank vote.
 */
export const BLANK_VOTE: Uint8Array = Buffer.from('BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', 'hex');

/**
 * Approximate ~1 sat/byte fee for transaction casting a vote, assuming a
 * transaction with 1 input + 1 output.
 */
export const DEFAULT_CAST_TX_FEE = 500;
