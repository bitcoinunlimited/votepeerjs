import { getExampleElection } from './testutil';
import {
  deriveContractAddress, createVoteMessage, signVoteMessage,
  twoOptionContractRedeemscript, calculateProposalID,
} from './primitives';
import { hash160Salted, generatePrivateKey } from '../crypto';

test('deriveContractAddress', async () => {
  const election = await getExampleElection();
  const [id, optionA, optionB] = await Promise.all([
    calculateProposalID(election),
    hash160Salted(election.salt, Buffer.from(election.optionA)),
    hash160Salted(election.salt, Buffer.from(election.optionB))]);
  expect(await deriveContractAddress(id, optionA, optionB, election.votersPKH[0]))
    .toEqual('bitcoincash:prx7fmgt7rav309qxjnxdw6zhran36t9dcvpyhsw4j');

  expect(await deriveContractAddress(id, optionA, optionB, election.votersPKH[1]))
    .toEqual('bitcoincash:pqj48avrnj5wzshf9ye7rqu5m3v3wj3cj5439m3w0d');

  expect(await deriveContractAddress(id, optionA, optionB, election.votersPKH[2]))
    .toEqual('bitcoincash:pzrpg8yrvrwsqkk8rzwu4mjncmcxgt0spv2pjfxcfs');
});

test('createVoteMessage', async () => {
  const salt = Buffer.from('unittest');
  const id = await hash160Salted(salt, Buffer.from('Proposal ID'));
  const vote = await hash160Salted(salt, Buffer.from('Vote option'));

  const message = createVoteMessage(id, vote);
  expect(message.length).toBe(40);
  expect(message.subarray(0, 20)).toStrictEqual(Buffer.from(id));
  expect(message.subarray(20)).toStrictEqual(Buffer.from(vote));
});

test('signVoteMessage', async () => {
  const salt = Buffer.from('unittest');
  const id = await hash160Salted(salt, Buffer.from('Proposal ID'));
  const vote = await hash160Salted(salt, Buffer.from('Vote option'));
  const message = createVoteMessage(id, vote);

  const privatekey = generatePrivateKey();
  const signature = await signVoteMessage(privatekey, message);

  // This framework produces Schnorr signature
  expect(signature.length).toBe(64);
});

test('test_twoOptionContractRedeemscript', async () => {
  const election = await getExampleElection();
  const [id, optionA, optionB] = await Promise.all([
    calculateProposalID(election),
    hash160Salted(election.salt, Buffer.from(election.optionA)),
    hash160Salted(election.salt, Buffer.from(election.optionB))]);

  const redeemScript = twoOptionContractRedeemscript(
    id, optionA, optionB, election.votersPKH[0],
  );

  expect(redeemScript.toString('hex')).toBe('14485beda9a544ee01effccef8d2be1e2778f5997e14a7557a06649059c24c7f8b13b955a6550444aa24147563cd871bc9af8f5bcd89a298f94d03135185f5140e2f5d8a017c4983cb56320d18f66bf01587aa445479a988547a5479ad557a5579557abb537901147f75537a887b01147f77767b8778537a879b7c14beefffffffffffffffffffffffffffffffffffff879b');
});
