import { OpcodesCommon, Transaction } from '@bitauth/libauth';
import util from 'util';
import assert from 'assert';
import Blockchain from '../blockchain';
import NoVoteError from '../error/novoteerror';
import {
  OP_DATA_40, OP_DATA_33, throwUnlessOpcode,
} from '../script';
import { BLANK_VOTE } from './votedef';
import { twoOptionContractRedeemscript } from './primitives';
import InvalidVoteError from '../error/invalidvoteerror';
import { isPlausibleSignatureSize } from '../transaction';
import NetworkTransaction from '../networktransaction';

/**
  * Fetches a voting transaction from contract address.
  * A voting transaction is identified as a spend from said address.
  *
  * @param chain - Electrum connection
  * @param contractAddr - A two-option-vote contract address
  * @param endHeight - Transaction must have been been confirmed at or before this height.
  * @param includeUnconfirmed - Include unconfirmed transactions
  */
export async function fetchVoteTx(
  chain: Blockchain, contractAddr: string, endHeight: number, includeUnconfirmed: boolean,
): Promise<NetworkTransaction> {
  const txs = await chain.getSpendingTxs(
    contractAddr, 0, endHeight, includeUnconfirmed,
  );

  if (txs.length === 0) {
    throw new NoVoteError();
  }

  txs.sort((a, b) => {
    // Unconfirmed (height <= 0)  are "at the biggest block height", so
    // give them a bump.
    assert(a.height !== undefined && b.height !== undefined);
    const heightA = a.height <= 0 ? 0xEEF00D : a.height;
    const heightB = b.height <= 0 ? 0xEEF00D : b.height;

    return heightA - heightB;
  });

  let best: any = null;
  for (const tx of txs) {
    if (best === null) {
      best = tx;
      continue;
    }
    if (tx.height === best.height) {
      throw new InvalidVoteError('Multiple spends at lowest height');
    }
    assert(tx.height > best.height || tx.height <= 0);
    return best;
  }
  return best;
}

/**
 * Parses a vote option from a two-option-vote transaction.
 *
 * As belt and suspender approach, it also validates that the transaction
 * contains the passed vote options, proposal ID and voterPKH.
 */
export function parseVoteFromTx(
  tx: Transaction,
  proposalID: Uint8Array,
  optionA: Uint8Array,
  optionB: Uint8Array,
  voterPKH: Uint8Array,
): [Uint8Array, Uint8Array] {
  if (tx.inputs.length > 1) {
    throw new InvalidVoteError('Multiple inputs NYI');
  }

  const script = tx.inputs[0].unlockingBytecode;
  let pos = 0;

  // push msg signature size
  const msgSigSize = script[pos++];
  if (!isPlausibleSignatureSize(msgSigSize)) {
    throw new InvalidVoteError(util.format(
      'Unexpected message signature size (%d)', msgSigSize,
    ));
  }
  const signature = script.slice(pos, pos + msgSigSize);
  // skip signature for message
  pos += msgSigSize;

  // message
  throwUnlessOpcode(script, pos++, OP_DATA_40);
  const msg = script.slice(pos, pos += OP_DATA_40);

  const id = Buffer.from(msg.slice(0, 20));
  const option = Buffer.from(msg.slice(20));

  if (!id.equals(proposalID)) {
    throw new InvalidVoteError(util.format(
      "Wrong proposal ID, expected '%s', got '%s'",
      proposalID, id,
    ));
  }

  if (option.compare(optionA) !== 0
        && option.compare(optionB) !== 0
        && option.compare(BLANK_VOTE) !== 0) {
    throw new InvalidVoteError(util.format(
      "Invalid vote option '%s'", option.toString('hex'),
    ));
  }

  // push tx signature size
  const txSigSize = script[pos++];
  // tx signature
  if (!isPlausibleSignatureSize(msgSigSize)) {
    throw new InvalidVoteError(util.format(
      'Unexpected tx signature size (%d)', txSigSize,
    ));
  }
  // skip signature
  pos += txSigSize;

  // push 33 - <compressed public key>
  throwUnlessOpcode(script, pos++, OP_DATA_33);
  // skip compressed public key
  pos += OP_DATA_33;

  // OP_PUSHDATA1
  throwUnlessOpcode(script, pos++, OpcodesCommon.OP_PUSHDATA_1);
  pos++; // skip redeemScript length

  const redeemScript = twoOptionContractRedeemscript(
    proposalID, optionA, optionB, voterPKH,
  );

  // the rest of the scriptSig should be the redeemScript
  if (redeemScript.compare(script.slice(pos)) !== 0) {
    throw new InvalidVoteError(util.format(
      "Wrong redeem script, expected '%s', got '%s'",
      redeemScript.toString('hex'),
      Buffer.from(script.slice(pos)).toString('hex'),
    ));
  }

  return [option, signature];
}
