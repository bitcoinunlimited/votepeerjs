import {
  encodeCashAddress, CashAddressNetworkPrefix, CashAddressType,
} from '@bitauth/libauth';
import { calculateProposalID } from './primitives';
import { hash160Salted, generatePrivateKey } from '../crypto';
import { TwoOptionVote } from './twooptionvote';

const assert = require('assert');

/**
 * Creates a random bitcoin cash P2PKH address
 */
export function createRandomAddress(): string {
  return encodeCashAddress(CashAddressNetworkPrefix.mainnet, CashAddressType.P2PKH,
    generatePrivateKey());
}

/**
 * Example election for unit testing.
 *
 * A vote with the default salt and endHeight exist on the BCH blockchain.
 *
 * @param salt - Election salt
 * @param endHeight - Blockchain tip height
 */
export async function getExampleElection(
  salt: Uint8Array = Buffer.from('unittest'),
  endHeight: number = 642042,
): Promise<TwoOptionVote> {
  const votersPKH: Uint8Array[] = [
    Buffer.from('0e2f5d8a017c4983cb56320d18f66bf01587aa44', 'hex'),
    Buffer.from('3790bb07029831ec90f8eb1ed7c1c09aac178185', 'hex'),
    Buffer.from('aaa8a14f0658c44809580b24299a592d7623976c', 'hex'),
    Buffer.from('9b1772d9287b9a3587b0a1e147f34714697c780c', 'hex'),
    Buffer.from('ca9c60699d3d4c6b71b1fa5b71934feaf9f5a004', 'hex'),
    Buffer.from('f7d1358c4baa20b31e5f84c238e964473d733f75', 'hex'),
  ];

  const election: TwoOptionVote = {
    network: 'mainnet',
    salt,
    description: 'Foo?',
    optionA: 'Bar',
    optionB: 'Baz',
    votersPKH,
    endHeight,
  };

  if (Buffer.from(salt).compare(Buffer.from('unittest')) === 0) {
    const [id, a, b] = await Promise.all([
      calculateProposalID(election),
      hash160Salted(election.salt, Buffer.from(election.optionA)),
      hash160Salted(election.salt, Buffer.from(election.optionB))]);
    assert(Buffer.from(id).toString('hex')
           === '485beda9a544ee01effccef8d2be1e2778f5997e');
    assert(Buffer.from(a).toString('hex')
           === '7563cd871bc9af8f5bcd89a298f94d03135185f5');
    assert(Buffer.from(b).toString('hex')
           === 'a7557a06649059c24c7f8b13b955a6550444aa24');
  }

  return election;
}
