import RingSignatureVote from './ringsignaturevote';
import Blockchain from '../blockchain';
import { fetchRingSignatureVotes } from './ringsignaturevotetally';

describe('ringSignatureVoteTally', () => {
  test('tally', async () => {
    const election = new RingSignatureVote(
    // Salt is *not* hex encoded.
      Buffer.from('51f20e3f84998a41bef6b768394726d0403c935c046f3d53e494f1553b442ea6'),
      'Anon',
      700818,
      709755,
      ['A', 'B'],
      [
      // bitcoincash:qrq08q63ymlxn95f9ptyq65jjwekzsdl2gp8rde6j0
        Uint8Array.from(Buffer.from(
          'a03d120c227b9c8cbe9fa3775b377358fcd8a756c45fedfa5c50bcb0cdb45864', 'hex',
        )),
        // bitcoincash:qqj3uzc9f8uzqgrdjhj609zqcdaj2h228uf9fae79k
        Uint8Array.from(Buffer.from(
          '6edcf341f28bba48e7701c93a60917f640b98b180110d09c5fa4ef24c5affc55', 'hex',
        )),
      ],
    );
    const electrum = new Blockchain();
    await electrum.connect();

    const ballots = await fetchRingSignatureVotes(electrum, election);
    expect(ballots.accepted.length).toBe(1);
    expect(ballots.accepted[0].vote).toStrictEqual(await election.getVoteOptionHashed(1));
    expect(ballots.accepted[0].transaction.getTxID()).toBe('35d22b71ac80df90f5b6837c00cf54d85d618ea28cadaa68ded83371f129acd2');
    expect(ballots.rejected.length).toBe(20);

    const ballotsIgnoringRejected = await fetchRingSignatureVotes(
      electrum,
      election,
      false, /* include rejected */
    );
    electrum.disconnect();
    expect(ballotsIgnoringRejected.rejected.length).toBe(0);
  });
});
