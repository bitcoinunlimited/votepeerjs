import Blockchain from '../blockchain';
import NetworkTransaction from '../networktransaction';
import RingSignatureVote from './ringsignaturevote';
import { PayloadContract } from '../payloadcontract';
import RingSignatureVoteValidator from './ringsignaturevotevalidator';
import {
  AcceptedBallotTransaction,
  RejectedBallotTransaction,
  BallotResult,
} from '../tally';
import { allIgnoringErrors } from '../utillist';

function sortVoteTransactions(transactions: NetworkTransaction[]): NetworkTransaction[] {
  transactions.sort((a, b) => {
    // Sort transactions in mempool last.
    const aHeight = a.height <= NetworkTransaction.HEIGHT_UNCONFIRMED
      ? 0xEEF00D
      : a.height;
    const bHeight = a.height <= NetworkTransaction.HEIGHT_UNCONFIRMED
      ? 0xEEF00D
      : b.height;

    if (aHeight === bHeight) {
      // Heights are the same. Order by transaction ID.
      return Buffer.from(a.txIdBE).compare(b.txIdBE);
    }
    return aHeight < bHeight ? -1 : 1;
  });
  return transactions;
}

/**
 * Fetch ballots for ring signature vote from the blockchain.
 */
export async function fetchRingSignatureVotes(
  electrum: Blockchain,
  election: RingSignatureVote,
  includeRejected: boolean = true,
): Promise<BallotResult> {
  /* eslint-disable no-unused-expressions */

  const address = await election.getNotificationAddress();

  // Fetch transactions with votes
  const maybeVotes: NetworkTransaction[] = sortVoteTransactions(
    await electrum.getFundingTransactions(address),
  );

  // Read vote from transaction
  const payloads: (Uint8Array | null)[] = await allIgnoringErrors(
    maybeVotes.map((tx) => PayloadContract.getPayloadFromTx(tx.getTransaction())),
  );

  const accepted: AcceptedBallotTransaction[] = [];
  const rejected: RejectedBallotTransaction[] = [];

  const validator = await RingSignatureVoteValidator.make(
    await election.getElectionID(),
    election.participantPublicKeys,
  );
  /* eslint-disable-next-line max-len */
  const txWithPayload: [NetworkTransaction, Uint8Array | null][] = maybeVotes.map((tx, i) => [tx, payloads[i]]);

  // Tally votes
  for (const [transaction, payload] of txWithPayload) {
    if (payload === null) {
      includeRejected && rejected.push({
        transaction,
        reason: 'No payload in transaction',
        participant: null,
      });
      continue;
    }
    if (payload.length < (20 /* vote */ + 44 /* constant */ + 64 /* 1 participant */)) {
      includeRejected && rejected.push({
        transaction,
        reason: 'Too short payload',
        participant: null,
      });
      continue;
    }
    const vote = Uint8Array.from(payload.slice(0, 20));
    if (!await election.isValidVoteOptionHash(vote)) {
      includeRejected && rejected.push({
        transaction,
        reason: 'Invalid vote option for election',
        participant: null,
      });
      continue;
    }
    const signature = Uint8Array.from(payload.slice(20));
    const [acceptable, rejectedReason] = await validator.isAcceptable(
      vote, signature, accepted,
    );
    if (acceptable) {
      accepted.push({
        transaction,
        vote,
        signature,
        participant: null,
      });
    } else {
      includeRejected && rejected.push({
        transaction,
        reason: rejectedReason as string,
        participant: null,
      });
    }
  }
  validator.close();

  return {
    accepted,
    rejected,
  };
}
