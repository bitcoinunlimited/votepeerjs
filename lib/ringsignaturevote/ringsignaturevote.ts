import {
  CashAddressType,
  OpcodesCommon,
  encodeCashAddress,
  encodeDataPush,
} from '@bitauth/libauth';

import { hash160, hash160Salted } from '../crypto';
import { byteArrayComparator } from '../utillist';
import { numberToUint32BE } from '../utilserialize';

export default class RingSignatureVote {
    public readonly RINGSIG_PUBLIC_KEY_LENGTH = 32;

    public readonly salt: Uint8Array;

    public readonly description: string;

    public readonly beginHeight: number;

    public readonly endHeight: number;

    public readonly voteOptions: string[];

    public readonly participantPublicKeys: Uint8Array[];

    private cachedElectionID: Uint8Array | null = null;

    private cachedVoteOptionsHashed: Uint8Array[] | null = null;

    constructor(
      salt: Uint8Array,
      description: string,
      beginHeight: number,
      endHeight: number,
      voteOptions: string[],
      participantPublicKeys: Uint8Array[],
    ) {
      if (beginHeight < 0) {
        throw Error(`beginHeight must be >= 0 (got ${beginHeight})`);
      }
      if (endHeight < beginHeight) {
        throw Error(`endHeight must be >= beginHeight (${endHeight} < ${beginHeight})`);
      }
      if (salt.length === 0) {
        throw Error('Salt cannot be empty');
      }
      if (voteOptions.length === 0) {
        throw Error('voteOptions cannot be empty');
      }
      if (participantPublicKeys.length === 0) {
        throw Error('participantPublicKeys cannot be empty');
      }
      for (const p of participantPublicKeys) {
        if (p.length !== this.RINGSIG_PUBLIC_KEY_LENGTH) {
          throw Error(
            'Participant public key size is wrong. '
                    + `Expected ${this.RINGSIG_PUBLIC_KEY_LENGTH}, got ${p.length}.`,
          );
        }
      }
      this.salt = salt;
      this.description = description;
      this.beginHeight = beginHeight;
      this.endHeight = endHeight;
      this.voteOptions = voteOptions;
      this.participantPublicKeys = participantPublicKeys;
    }

    /**
     * Get the notification address for this election. All vote casting
     * transactions have an output to this notification address.
     */
    async getNotificationAddress(): Promise<string> {
      const hash = await hash160(await this.getNotificationRedeemScript());
      return encodeCashAddress('bitcoincash', CashAddressType.P2SH, hash);
    }

    /**
     * Get the redeem script for the notification address.
     */
    async getNotificationRedeemScript(): Promise<Uint8Array> {
      return Buffer.concat([
        encodeDataPush(await this.getElectionID()),
        Buffer.alloc(1, OpcodesCommon.OP_EQUALVERIFY),
      ]);
    }

    /**
     * The election ID is made out of a hash of all the data belonging to a vote.
     */
    async getElectionID(): Promise<Uint8Array> {
      if (this.cachedElectionID !== null) {
        return this.cachedElectionID;
      }

      const { salt } = this;
      const voteOptionsHashed = await Promise.all(
        this.voteOptions.map((o) => hash160(Buffer.concat([salt, Buffer.from(o)]))),
      );

      voteOptionsHashed.sort(byteArrayComparator);
      const participants = this.participantPublicKeys;
      participants.sort(byteArrayComparator);

      const electionData = Buffer.concat([
        salt,
        Buffer.from(this.description),
        numberToUint32BE(this.beginHeight),
        numberToUint32BE(this.endHeight),
        Buffer.concat(voteOptionsHashed),
        Buffer.concat(participants),
      ]);
      this.cachedElectionID = await hash160(electionData);
      return this.cachedElectionID;
    }

    /**
     * Hash all vote options and cache the result.
     */
    private async cacheVoteOptionHashes(): Promise<void> {
      if (this.cachedVoteOptionsHashed !== null) {
        return;
      }
      this.cachedVoteOptionsHashed = await Promise.all(
        this.voteOptions.map((o) => hash160Salted(this.salt, Buffer.from(o))),
      );
    }

    /**
     * Get hash for vote option at index.
     * @param optionIndex - Index for a vote option.
     */
    async getVoteOptionHashed(optionIndex: number): Promise<Uint8Array> {
      await this.cacheVoteOptionHashes();
      return this.cachedVoteOptionsHashed![optionIndex];
    }

    /**
     * Check if a vote option hash is valid for this contract.
     * @param hash - Hashed vote option
     */
    async isValidVoteOptionHash(hash: Uint8Array): Promise<boolean> {
      await this.cacheVoteOptionHashes();
      const asBuffer = Buffer.from(hash);
      for (const h of this.cachedVoteOptionsHashed!) {
        if (asBuffer.equals(h)) {
          return true;
        }
      }
      return false;
    }

    /**
     * Given a vote option hash, get the vote option represented
     * by this hash.
     * @param hash - Hashed vote option
     */
    async getOptionFromHash(hash: Uint8Array): Promise<string> {
      await this.cacheVoteOptionHashes();
      let i = 0;
      const asBuffer = Buffer.from(hash);
      for (const h of this.cachedVoteOptionsHashed!) {
        if (asBuffer.equals(h)) {
          return this.voteOptions[i];
        }
        ++i;
      }
      throw Error('Not a valid vote option hash for election');
    }
}
