import { reverseBuffer } from './utillist';

describe('utillist', () => {
  it('reverseBuffer', async () => {
    const buffer = Buffer.from([0xaa, 0xbb, 0xcc]);

    expect(reverseBuffer(buffer)).toStrictEqual(
      Uint8Array.from([0xcc, 0xbb, 0xaa]),
    );
  });
});
