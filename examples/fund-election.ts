import {
    TwoOptionVote, TwoOptionVoteContract, Blockchain,
     derivePublicKey, getPublicKeyHash,


     hashToAddress,
     waitForBalance,
     getOrFundUtxo
  } from '@bitcoinunlimited/votepeerjs';
  const sleep = require('sleep')

/**
 * In this example we fund all the voting contracts in the election so that
 * the voters don't need Bitcoin Cash to vote.
 */
 async function run() {
    const electrum = new Blockchain();
    await electrum.connect();

  // Initialize participants.
  const alice = Buffer.alloc(32, 0xee);
  const bob = Buffer.alloc(32, 0xdd);

  // Initialize an election
  const votersPKH = [
    await getPublicKeyHash(await derivePublicKey(alice)),
    await getPublicKeyHash(await derivePublicKey(bob)),
  ];

  const election: TwoOptionVote = {
    network: 'mainnet',
    salt: Buffer.alloc(0),
    description: 'Pizza for lunch?',
    optionA: 'Yes',
    optionB: 'No',
    endHeight: 1_000_000,
    votersPKH,
  };


  const fundingPrivateKey = Buffer.alloc(32, 0xcc); // generatePrivateKey();
  const fundingAddress = hashToAddress(
      await getPublicKeyHash(await derivePublicKey(fundingPrivateKey)),
      'p2pkh');

    const requestedAmount = 600 * votersPKH.length; // satoshis
    await waitForBalance(electrum, fundingAddress, requestedAmount, (balance: number) => {
        console.log(`Waiting for ${requestedAmount} sats in ${fundingAddress}. Current amount: ${balance}`);
        sleep.sleep(1)
    });
    for (const voter of [alice, bob]) {
        const contract = await TwoOptionVoteContract.make(election, voter);
        const utxo = await getOrFundUtxo(electrum, await contract.getContractAddress(), 600, fundingPrivateKey);
        console.log(`Contract funded in utxo: `, utxo);
        sleep.sleep(2) // Allow tx to propagate
    }
    electrum.disconnect()

 }
run()
