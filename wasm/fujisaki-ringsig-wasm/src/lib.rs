extern crate bincode;
extern crate console_error_panic_hook;
extern crate fujisaki_ringsig;
extern crate wasm_bindgen;
extern crate wee_alloc;

// Use `wee_alloc` as the global allocator. Less efficient, but reduces
// wasm size.
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

use fujisaki_ringsig::{key, sig, trace};
use std::panic;
use wasm_bindgen::prelude::*;

/// For data structures that we don't export. This hides it behind a void
/// pointer.
#[wasm_bindgen]
pub struct OpaquePtr(*mut std::ffi::c_void);

#[wasm_bindgen]
pub fn init_wasm_bindings() {
    panic::set_hook(Box::new(console_error_panic_hook::hook));
}

/// Initialize a `Tag` with an issue and no public keys.
#[wasm_bindgen]
pub fn wasm_init_tag(issue: Vec<u8>) -> OpaquePtr {
    let tag = sig::Tag {
        issue,
        pubkeys: vec![],
    };
    OpaquePtr(Box::into_raw(Box::new(tag)) as *mut _)
}

/// Add a public key to a tag.
#[wasm_bindgen]
pub fn wasm_tag_add_pubkey(tag: &OpaquePtr, pubkey: Vec<u8>) -> bool {
    let pubkey = key::PublicKey::from_bytes(&pubkey).expect("pubkey from bytes");

    let ptr = tag.0 as *mut sig::Tag;
    return unsafe {
        match ptr.as_mut() {
            Some(tag) => {
                tag.pubkeys.push(pubkey);
                true
            }
            None => false,
        }
    };
}

#[wasm_bindgen]
pub fn wasm_verify(msg: Vec<u8>, tag: &OpaquePtr, sig: Vec<u8>) -> bool {
    let tag_ptr = tag.0 as *mut sig::Tag;
    let sig = bincode::deserialize(&sig);

    let sig = match sig {
        Ok(sig) => sig,
        Err(_) => return false,
    };

    unsafe {
        match tag_ptr.as_mut() {
            Some(tag) => sig::verify(&msg, tag, &sig),
            None => false,
        }
    }
}

// Maps trace::Trace, but without PublicKey as part of the enum.
/// Encodes the relationship of two signatures
#[wasm_bindgen]
pub enum TraceResult {
    /// `Indep` indicates that the two given signatures were constructed with different private
    /// keys.
    Indep,

    /// `Linked` indicates that the same private key was used to sign the same message under the
    /// same tag. This does not reveal which key performed the double-signature.
    Linked,

    /// The same key was used to sign distinct messages under the same tag. `pubkey_out` reveales
    /// that pubkey.
    Revealed,

    // We can also add error conditions
    InputErrorSig1,
    InputErrorSig2,
    InputErrorTag,
}

#[wasm_bindgen]
pub fn wasm_trace(
    msg1: Vec<u8>,
    sig1: Vec<u8>,
    msg2: Vec<u8>,
    sig2: Vec<u8>,
    tag: &OpaquePtr,
) -> TraceResult {
    let tag_ptr = tag.0 as *mut sig::Tag;
    let sig1 = match bincode::deserialize(&sig1) {
        Ok(s) => s,
        Err(_) => return TraceResult::InputErrorSig1,
    };
    let sig2 = match bincode::deserialize(&sig2) {
        Ok(s) => s,
        Err(_) => return TraceResult::InputErrorSig2,
    };

    let tag = unsafe {
        match tag_ptr.as_mut() {
            Some(t) => t,
            None => return TraceResult::InputErrorTag,
        }
    };
    match trace::trace(&msg1, &sig1, &msg2, &sig2, tag) {
        trace::Trace::Indep => TraceResult::Indep,
        trace::Trace::Linked => TraceResult::Linked,
        trace::Trace::Revealed(_pubkey) => TraceResult::Revealed,
    }
}

#[wasm_bindgen]
pub fn wasm_free_tag(ptr: OpaquePtr) {
    unsafe {
        Box::from_raw(ptr.0 as *mut sig::Tag);
    }
}
