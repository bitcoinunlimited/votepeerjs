/* tslint:disable */
/* eslint-disable */
/**
*/
export function init_wasm_bindings(): void;
/**
* Initialize a `Tag` with an issue and no public keys.
* @param {Uint8Array} issue
* @returns {OpaquePtr}
*/
export function wasm_init_tag(issue: Uint8Array): OpaquePtr;
/**
* Add a public key to a tag.
* @param {OpaquePtr} tag
* @param {Uint8Array} pubkey
* @returns {boolean}
*/
export function wasm_tag_add_pubkey(tag: OpaquePtr, pubkey: Uint8Array): boolean;
/**
* @param {Uint8Array} msg
* @param {OpaquePtr} tag
* @param {Uint8Array} sig
* @returns {boolean}
*/
export function wasm_verify(msg: Uint8Array, tag: OpaquePtr, sig: Uint8Array): boolean;
/**
* @param {Uint8Array} msg1
* @param {Uint8Array} sig1
* @param {Uint8Array} msg2
* @param {Uint8Array} sig2
* @param {OpaquePtr} tag
* @returns {number}
*/
export function wasm_trace(msg1: Uint8Array, sig1: Uint8Array, msg2: Uint8Array, sig2: Uint8Array, tag: OpaquePtr): number;
/**
* @param {OpaquePtr} ptr
*/
export function wasm_free_tag(ptr: OpaquePtr): void;
/**
* Encodes the relationship of two signatures
*/
export enum TraceResult {
/**
* `Indep` indicates that the two given signatures were constructed with different private
* keys.
*/
  Indep,
/**
* `Linked` indicates that the same private key was used to sign the same message under the
* same tag. This does not reveal which key performed the double-signature.
*/
  Linked,
/**
* The same key was used to sign distinct messages under the same tag. `pubkey_out` reveales
* that pubkey.
*/
  Revealed,
  InputErrorSig1,
  InputErrorSig2,
  InputErrorTag,
}
/**
* For data structures that we don't export. This hides it behind a void
* pointer.
*/
export class OpaquePtr {
  free(): void;
}
