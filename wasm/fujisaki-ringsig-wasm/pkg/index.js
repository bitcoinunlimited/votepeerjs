let imports = {};
imports['__wbindgen_placeholder__'] = module.exports;
let wasm;
const { TextDecoder, TextEncoder } = require(`util`);

const heap = new Array(32).fill(undefined);

heap.push(undefined, null, true, false);

function getObject(idx) { return heap[idx]; }

let heap_next = heap.length;

function dropObject(idx) {
    if (idx < 36) return;
    heap[idx] = heap_next;
    heap_next = idx;
}

function takeObject(idx) {
    const ret = getObject(idx);
    dropObject(idx);
    return ret;
}

let cachedTextDecoder = new TextDecoder('utf-8', { ignoreBOM: true, fatal: true });

cachedTextDecoder.decode();

let cachegetUint8Memory0 = null;
function getUint8Memory0() {
    if (cachegetUint8Memory0 === null || cachegetUint8Memory0.buffer !== wasm.memory.buffer) {
        cachegetUint8Memory0 = new Uint8Array(wasm.memory.buffer);
    }
    return cachegetUint8Memory0;
}

function getStringFromWasm0(ptr, len) {
    return cachedTextDecoder.decode(getUint8Memory0().subarray(ptr, ptr + len));
}
/**
*/
module.exports.init_wasm_bindings = function() {
    wasm.init_wasm_bindings();
};

let WASM_VECTOR_LEN = 0;

function passArray8ToWasm0(arg, malloc) {
    const ptr = malloc(arg.length * 1);
    getUint8Memory0().set(arg, ptr / 1);
    WASM_VECTOR_LEN = arg.length;
    return ptr;
}
/**
* Initialize a `Tag` with an issue and no public keys.
* @param {Uint8Array} issue
* @returns {OpaquePtr}
*/
module.exports.wasm_init_tag = function(issue) {
    var ptr0 = passArray8ToWasm0(issue, wasm.__wbindgen_malloc);
    var len0 = WASM_VECTOR_LEN;
    var ret = wasm.wasm_init_tag(ptr0, len0);
    return OpaquePtr.__wrap(ret);
};

function _assertClass(instance, klass) {
    if (!(instance instanceof klass)) {
        throw new Error(`expected instance of ${klass.name}`);
    }
    return instance.ptr;
}
/**
* Add a public key to a tag.
* @param {OpaquePtr} tag
* @param {Uint8Array} pubkey
* @returns {boolean}
*/
module.exports.wasm_tag_add_pubkey = function(tag, pubkey) {
    _assertClass(tag, OpaquePtr);
    var ptr0 = passArray8ToWasm0(pubkey, wasm.__wbindgen_malloc);
    var len0 = WASM_VECTOR_LEN;
    var ret = wasm.wasm_tag_add_pubkey(tag.ptr, ptr0, len0);
    return ret !== 0;
};

/**
* @param {Uint8Array} msg
* @param {OpaquePtr} tag
* @param {Uint8Array} sig
* @returns {boolean}
*/
module.exports.wasm_verify = function(msg, tag, sig) {
    var ptr0 = passArray8ToWasm0(msg, wasm.__wbindgen_malloc);
    var len0 = WASM_VECTOR_LEN;
    _assertClass(tag, OpaquePtr);
    var ptr1 = passArray8ToWasm0(sig, wasm.__wbindgen_malloc);
    var len1 = WASM_VECTOR_LEN;
    var ret = wasm.wasm_verify(ptr0, len0, tag.ptr, ptr1, len1);
    return ret !== 0;
};

/**
* @param {Uint8Array} msg1
* @param {Uint8Array} sig1
* @param {Uint8Array} msg2
* @param {Uint8Array} sig2
* @param {OpaquePtr} tag
* @returns {number}
*/
module.exports.wasm_trace = function(msg1, sig1, msg2, sig2, tag) {
    var ptr0 = passArray8ToWasm0(msg1, wasm.__wbindgen_malloc);
    var len0 = WASM_VECTOR_LEN;
    var ptr1 = passArray8ToWasm0(sig1, wasm.__wbindgen_malloc);
    var len1 = WASM_VECTOR_LEN;
    var ptr2 = passArray8ToWasm0(msg2, wasm.__wbindgen_malloc);
    var len2 = WASM_VECTOR_LEN;
    var ptr3 = passArray8ToWasm0(sig2, wasm.__wbindgen_malloc);
    var len3 = WASM_VECTOR_LEN;
    _assertClass(tag, OpaquePtr);
    var ret = wasm.wasm_trace(ptr0, len0, ptr1, len1, ptr2, len2, ptr3, len3, tag.ptr);
    return ret >>> 0;
};

/**
* @param {OpaquePtr} ptr
*/
module.exports.wasm_free_tag = function(ptr) {
    _assertClass(ptr, OpaquePtr);
    var ptr0 = ptr.ptr;
    ptr.ptr = 0;
    wasm.wasm_free_tag(ptr0);
};

function addHeapObject(obj) {
    if (heap_next === heap.length) heap.push(heap.length + 1);
    const idx = heap_next;
    heap_next = heap[idx];

    heap[idx] = obj;
    return idx;
}

let cachedTextEncoder = new TextEncoder('utf-8');

const encodeString = (typeof cachedTextEncoder.encodeInto === 'function'
    ? function (arg, view) {
    return cachedTextEncoder.encodeInto(arg, view);
}
    : function (arg, view) {
    const buf = cachedTextEncoder.encode(arg);
    view.set(buf);
    return {
        read: arg.length,
        written: buf.length
    };
});

function passStringToWasm0(arg, malloc, realloc) {

    if (realloc === undefined) {
        const buf = cachedTextEncoder.encode(arg);
        const ptr = malloc(buf.length);
        getUint8Memory0().subarray(ptr, ptr + buf.length).set(buf);
        WASM_VECTOR_LEN = buf.length;
        return ptr;
    }

    let len = arg.length;
    let ptr = malloc(len);

    const mem = getUint8Memory0();

    let offset = 0;

    for (; offset < len; offset++) {
        const code = arg.charCodeAt(offset);
        if (code > 0x7F) break;
        mem[ptr + offset] = code;
    }

    if (offset !== len) {
        if (offset !== 0) {
            arg = arg.slice(offset);
        }
        ptr = realloc(ptr, len, len = offset + arg.length * 3);
        const view = getUint8Memory0().subarray(ptr + offset, ptr + len);
        const ret = encodeString(arg, view);

        offset += ret.written;
    }

    WASM_VECTOR_LEN = offset;
    return ptr;
}

let cachegetInt32Memory0 = null;
function getInt32Memory0() {
    if (cachegetInt32Memory0 === null || cachegetInt32Memory0.buffer !== wasm.memory.buffer) {
        cachegetInt32Memory0 = new Int32Array(wasm.memory.buffer);
    }
    return cachegetInt32Memory0;
}
/**
* Encodes the relationship of two signatures
*/
module.exports.TraceResult = Object.freeze({
/**
* `Indep` indicates that the two given signatures were constructed with different private
* keys.
*/
Indep:0,"0":"Indep",
/**
* `Linked` indicates that the same private key was used to sign the same message under the
* same tag. This does not reveal which key performed the double-signature.
*/
Linked:1,"1":"Linked",
/**
* The same key was used to sign distinct messages under the same tag. `pubkey_out` reveales
* that pubkey.
*/
Revealed:2,"2":"Revealed",InputErrorSig1:3,"3":"InputErrorSig1",InputErrorSig2:4,"4":"InputErrorSig2",InputErrorTag:5,"5":"InputErrorTag", });
/**
* For data structures that we don't export. This hides it behind a void
* pointer.
*/
class OpaquePtr {

    static __wrap(ptr) {
        const obj = Object.create(OpaquePtr.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_opaqueptr_free(ptr);
    }
}
module.exports.OpaquePtr = OpaquePtr;

module.exports.__wbg_new_693216e109162396 = function() {
    var ret = new Error();
    return addHeapObject(ret);
};

module.exports.__wbg_stack_0ddaca5d1abfb52f = function(arg0, arg1) {
    var ret = getObject(arg1).stack;
    var ptr0 = passStringToWasm0(ret, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
    var len0 = WASM_VECTOR_LEN;
    getInt32Memory0()[arg0 / 4 + 1] = len0;
    getInt32Memory0()[arg0 / 4 + 0] = ptr0;
};

module.exports.__wbg_error_09919627ac0992f5 = function(arg0, arg1) {
    try {
        console.error(getStringFromWasm0(arg0, arg1));
    } finally {
        wasm.__wbindgen_free(arg0, arg1);
    }
};

module.exports.__wbindgen_object_drop_ref = function(arg0) {
    takeObject(arg0);
};

module.exports.__wbindgen_throw = function(arg0, arg1) {
    throw new Error(getStringFromWasm0(arg0, arg1));
};

const path = require('path').join(__dirname, 'index_bg.wasm');
const bytes = require('fs').readFileSync(path);

const wasmModule = new WebAssembly.Module(bytes);
const wasmInstance = new WebAssembly.Instance(wasmModule, imports);
wasm = wasmInstance.exports;
module.exports.__wasm = wasm;

